/*************************************************************************/
/**********************研修理解度チェック*********************************/
/*************************************************************************/

// Q1
// 実行されたときに表示されるアラートの文言を答えよ
function question1() {
	var x = 10;
	if (x % 2 === 1) {
		alert("true");
	} else {
		alert("false");
	}
}

// Q2
// アラートに順番に表示される数値を答えよ。
function question2() {
	var x = 0;
	for(x = 0; x < 5; x++) {
		alert((2 * x) + 1);
	}
}

// Q3
// アラートに順番に表示される数値を答えよ。
function question3() {
	var array = [1,2,3,4,5]
	for(var index = 4; index >= 0; index--) {
		alert(array[index]);
	}
}

// Q4
// アラートに表示される文言を答えよ。
function question4() {
	var weekly =  [ "日", "月", "火", "水", "木", "金", "土" ]
	var date = new Date();
	var dayOfWeek = date.getDay();
	alert(weekly[dayOfWeek]);
}

// Q5
// アラートに表示される数値を答えよ。
function question5() {
	// 関数�@呼び出し
	alert(calcMethod1(120, 80));
}
// 関数�@
function calcMethod1(x, y) {
	var result = (x + y) / 2;
	return result;
}

// Q6
// アラートに表示される数値を答えよ。
function question6() {
	// 関数�A呼び出し
	alert(calcMethod2(10) * 1.5);
}
// 関数�A
function calcMethod2(x) {
	var result = x * 10
	return result;
}



